import { Component, provide } from '@angular/core';
import { ionicBootstrap, Platform } from 'ionic-angular';
import { StatusBar } from 'ionic-native';
import { Locker, LockerConfig } from 'angular2-locker'

import { FIREBASE_CONFIG } from './config/constants.config';
import { AuthService } from './services/auth';
import { HomeComponent } from './pages/home';

import {
    FIREBASE_PROVIDERS,
    defaultFirebase,
    WindowLocation
} from 'angularfire2';

@Component({
    template: '<ion-nav [root]="rootPage"></ion-nav>',
})
export class MyApp {

    rootPage: any = HomeComponent;

    constructor(platform: Platform) {

        platform.ready().then(() => {

            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            StatusBar.styleDefault();

        });

    }
    
}

ionicBootstrap(MyApp, [
    FIREBASE_PROVIDERS,
    defaultFirebase(FIREBASE_CONFIG),
    provide(WindowLocation, {
        useValue: {
            hash: '',
            search: '',
            pathname: '/',
            port: '',
            hostname: 'localhost',
            host: 'localhost',
            protocol: 'https',
            origin: 'localhost',
            href: 'https://localhost/'
        }
    }),
    AuthService,
    provide(LockerConfig, {
        useValue: new LockerConfig('ShoppingList', Locker.DRIVERS.LOCAL)
    }), 
    Locker
], {
    swipeBackEnabled: false
});
