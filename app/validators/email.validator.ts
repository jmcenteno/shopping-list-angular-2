import { FormControl } from '@angular/forms';
import * as validator from 'validator';

export function EmailValidator (control: FormControl) {

	return control.value == '' || validator.isEmail(control.value) ? null : {
		email: {
			valid: false
		}
	};

}
