import { Component, OnInit } from '@angular/core';

import { 
    REACTIVE_FORM_DIRECTIVES,
    FormBuilder,
    FormControl,
    FormGroup,
    Validators
} from '@angular/forms';

import {
    NavController,
    ViewController,
    NavParams,
    AlertController,
    LoadingController,
    ModalController
} from 'ionic-angular';

import {
    AngularFire,
    AuthProviders,
    AuthMethods,
    FirebaseAuthState
} from 'angularfire2';

import { APP_CONFIG } from '../../config/constants.config';
import { AuthService } from '../../services/auth';
import { ListsComponent } from '../shopping-list';
import { EmailValidator } from '../../validators';
import { RegisterComponent } from '../register';

@Component({
    templateUrl: APP_CONFIG.paths.templates + '/login/login.template.html',
    directives: [REACTIVE_FORM_DIRECTIVES],
    providers: [AuthService]
})
export class LoginPage {

    appName: string = APP_CONFIG.name;
    error: any;
    loginForm: FormGroup;
    loader: any;
    submitted: boolean = false;
    registerModal: any;

    constructor(
        public viewCtrl: ViewController,
        private alertController: AlertController,
        private loadingController: LoadingController,
        private formBuilder: FormBuilder,
        private navParams: NavParams,
        private nav: NavController,
        private modal: ModalController,
        private auth: AuthService) {}

    ngOnInit () {

        this.loginForm = this.formBuilder.group({
            email: new FormControl('', Validators.compose([
                Validators.required, 
                EmailValidator
            ])),
            password: new FormControl('', Validators.compose([
                Validators.required, 
                Validators.minLength(5)
            ]))
        });

    }
    
    login (method: string = 'email', e?: Event) {

        this.submitted = true;

        this.loader = this.loadingController.create({
            content: 'Please wait...'
        });

        //this.loader.present();

        switch (method) {

            case 'facebook' :
                this.facebookLogin();
                break;

            case 'twitter' :
                this.twitterLogin();
                break;

            case 'google' :
                this.googleLogin();
                break;

            default :
                e.preventDefault();
                this.emailLogin();

        }
        

    }

    /**
     * this logs in the user using the form credentials.
     * 
     * if the user is a new user, then we need to create the user AFTER
     * we have successfully logged in
     * 
     * @param _credentials {Object} the email and password from the form
     */
    private emailLogin () {

        if (!this.loginForm.valid) {
            return;
        }

        // login using the email/password auth provider
        this.auth.emailLogin(this.loginForm.value)
            .then((authData) => {
        
                this.submitted = false;
                //this.loader.dismiss();

            })
            .catch((error) => {

                this.submitted = false;
                //this.loader.dismiss();

                let errorMessage = 'An error has occured. Please try again.';

                switch (error['code']) {

                    case 'auth/user-not-found':
                        errorMessage = 'Invalid username or password.';
                        break;

                    case 'auth/wrong-password':
                        errorMessage = 'Invalid username or password.';
                        break;

                }

                this.displayAlert(errorMessage);
                console.log(error);

            });

    }

    private facebookLogin () {

        this.auth.facebookLogin()
            .then((authData) => {

                this.submitted = false;

            })
            .catch((error) => {

                this.submitted = false;
                //this.loader.dismiss();

            });

    }

    private twitterLogin() {
        
        this.auth.twitterLogin()
            .then((authData) => {
                
                this.submitted = false;
                console.log('authData 2', authData)

            })
            .catch((error) => {
                
                this.submitted = false;
                console.log('twitter auth', error)

            });

    }

    private googleLogin () {

        this.submitted = true;

        this.auth.googleLogin()
            .then((authData) => {

                this.submitted = false;

            })
            .catch((error) => {

                this.submitted = false;
                console.log('google auth', error);
                //this.loader.dismiss();

            });

    }

    /**
     * this create in the user using the form credentials. 
     *
     * we are preventing the default behavor of submitting 
     * the form
     * 
     * @param _credentials {Object} the email and password from the form
     * @param _event {Object} the event information from the form submit
     */
    register () {

        this.registerModal = this.modal.create(RegisterComponent);
        this.registerModal.present();

        /*this.loader = this.loadingController.create({
            content: 'Please wait...'
        });

        //this.loader.present();

        this.auth.createUser(this.loginForm.value)
            .then((user) => {

                //this.loader.dismiss();
           
                console.log(`Create User Success:`, user);
                this.emailLogin();

            })
            .catch((error) => {

                //this.loader.dismiss();
                console.error('Create User Failure:', error);

                let errorMessage = 'An error has occured. Please try again.';

                switch (error['code']) {
                    
                    case 'auth/email-already-in-use':
                        errorMessage = error['message'];
                        break;
                    
                }

                this.displayAlert(errorMessage);

            });*/

    }

    private displayAlert (error: any) {

        let alert = this.alertController.create({
            title: 'Error!',
            subTitle: error,
            buttons: ['OK']
        });
    
        alert.present();
    
    }

}
