import { Injectable } from '@angular/core';
import { Locker } from 'angular2-locker';
import {
	AngularFire,
	FirebaseListObservable,
	FirebaseObjectObservable
} from 'angularfire2';

import { List } from './list.model';
import { Item } from './item.model';

@Injectable()
export class ShoppingListService {

	private user: any;
	private lists: any;

	constructor (
		private firebase: AngularFire,
		private localStorage: Locker) {

		this.user = this.localStorage.get('user');
		this.lists = this.firebase.database.list('/lists/' + this.user.uid);

	}

	getLists (): FirebaseListObservable<any> {

		return this.lists;

	}

	getListById (id: string): FirebaseObjectObservable<any> {

		let list = this.firebase.database.object(`/lists/${this.user.uid}/${id}`);
		return list;

	};

	createList (list: List) {

		this.lists.push(list);	

	};

	updateList (list: any) {

		console.log(`/lists/${this.user.uid}/${list.$key}`, list);
		let ref = this.firebase.database.object(`/lists/${this.user.uid}/${list.$key}/name`);
		ref.set(list.name);

	};

	deleteList (id: string) {

		let ref = this.firebase.database.object(`/lists/${this.user.uid}/${id}`);
		ref.remove();

	}

	getItemsByListId (listId: string): FirebaseListObservable<any> {

		let ref = this.firebase.database.list(`/lists/${this.user.uid}/${listId}/items`);
		return ref;

	}

	createItem (listId: string, data: any) {

		let item = new Item();

		for (let key in item) {
			item[key] = data[key] || null;
		}

		let ref = this.firebase.database.list(`/lists/${this.user.uid}/${listId}/items`);
		ref.push(item);

	}

	updateItem (listId: string, data: any) {

		let ref: any;

		ref = this.firebase.database.object(`/lists/${this.user.uid}/${listId}/items/${data.$key}/name`);
		ref.set(data.name);

		ref = this.firebase.database.object(`/lists/${this.user.uid}/${listId}/items/${data.$key}/checked`);
		ref.set(data.checked);		

	}

	updateAllItems (listId: string, items: any) {

		items.forEach((item, i) => {
			
			let ref = this.firebase.database.object(`/lists/${this.user.uid}/${listId}/items/${item.$key}`)
			delete item.$key;
			ref.update(item);

		});

		/*let ref = this.firebase.database.object(`/lists/${this.user.uid}/${listId}/`);
		
		data.forEach((item) => {
			delete item.$key;
		});

		ref.update({
			items: data
		});*/

	}

	deleteItem (listId: string, id: string) {

		let ref = this.firebase.database.object(`/lists/${this.user.uid}/${listId}/items/${id}`);
		ref.remove();

	}

}
