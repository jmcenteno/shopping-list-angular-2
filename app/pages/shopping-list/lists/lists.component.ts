import { Component, OnInit } from '@angular/core';
import { Page, NavController, AlertController, LoadingController, ActionSheetController } from 'ionic-angular';
import { AngularFire, FirebaseListObservable } from 'angularfire2';

import { APP_CONFIG } from '../../../config/constants.config';
import { ShoppingListService } from '../shopping-list.service';
import { List } from '../list.model';
import { ListDetailsComponent } from '../list-details';

@Page({
  templateUrl: APP_CONFIG.paths.templates + '/shopping-list/lists/lists.template.html',
  providers: [ShoppingListService]
})

export class ListsComponent implements OnInit {

	appName: string = APP_CONFIG.name;
	lists: Array<any> = [];
	loader: any;

	constructor (
		private nav: NavController, 
		private loading: LoadingController,
		private alert: AlertController,
		private actionSheet: ActionSheetController,
		private service: ShoppingListService) {}

	ngOnInit () {

		this.getLists();

	}

	selectList (id: string) {

		this.nav.push(ListDetailsComponent, { id: id });

	}

	showAddPopup () {

		let list = new List();

		let prompt = this.alert.create({
			title: 'New List',
			message: "Enter a name for this list",
			inputs: [
				{
					name: 'name',
					type: 'text',
					placeholder: 'List Name',
					value: list.name
				},
			],
			buttons: [
				{
					text: 'Cancel',
					handler: (data) => {
						console.log('Cancel clicked');
					}
				},
				{
					text: 'Save',
					handler: (data) => {

						if (data.name != '') {
							this.service.createList(data);
						}
						
					}
				}
			]
		});
	
		prompt.present();

	}

	showEditPopup (list) {

		let prompt = this.alert.create({
			title: 'Edit List',
			message: "Enter a name for this item",
			inputs: [
				{
					name: 'name',
					type: 'text',
					placeholder: 'List Name',
					value: list.name
				},
			],
			buttons: [
				{
					text: 'Cancel',
					handler: (data) => {
						console.log('Cancel clicked');
					}
				},
				{
					text: 'Save',
					handler: (data) => {

						if (data.name != '') {
							list.name = data.name;
							this.service.updateList(list);
						}
						
					}
				}
			]
		});
	
		prompt.present();

	}

	showDeleteActionSheet (id: string) {
		
		let actionSheet = this.actionSheet.create({
			title: 'Confirm Delete',
			buttons: [
				{
					text: 'Delete List',
					role: 'destructive',
					handler: () => {
						this.service.deleteList(id);
					}
				},
				{
					text: 'Cancel',
					role: 'cancel',
					handler: () => {
						console.log('Cancel clicked');
					}
				}
			]
		});
		
		actionSheet.present();

	}

	private getLists () {

		this.loader = this.loading.create({
			content: 'Please wait...'
		});

		this.loader.present();
		
		this.service.getLists().subscribe((snapshots) => {

			this.lists = snapshots.map((snapshot, i) => {
				
				let list = snapshot;

				if (snapshot.items) {

					list.items = (Object.keys(snapshot.items)).map((key) => {
						
						let item = snapshot.items[key];
						item.$key = key;

						return item;

					});

				}

				return list;

			});

			setTimeout(() => {
				this.loader.dismiss();
			}, 1000);

		});

	}

}
