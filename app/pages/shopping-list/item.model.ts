export class Item {

	name: string = '';
	qty: number = 1;
	checked: boolean = false;
	order: number = 0;
	created: Number = (new Date()).getTime();

}
