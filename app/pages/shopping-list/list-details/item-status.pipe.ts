import { Pipe, PipeTransform } from '@angular/core';
import { Item } from '../item.model';

@Pipe({
	name: 'checked'
})

export class ItemStatusPipe  {
	
	transform (items: Item[], status: boolean) {
		return items.filter((item) => {
			return (item.checked == status);
		});
	}

}
