import { Component, ViewChild } from '@angular/core';
import {
	NavParams, 
	NavController, 
	reorderArray, 
	AlertController, 
	LoadingController, 
	ActionSheetController, 
	PopoverController,
	Content 
} from 'ionic-angular';
import * as _ from 'lodash';

import { APP_CONFIG } from '../../../config/constants.config';
import { ShoppingListService } from '../shopping-list.service';
import { List } from '../list.model';
import { Item } from '../item.model';
import { ItemStatusPipe } from './item-status.pipe';
import { ListDetailsPopover } from './popover';

@Component({
	templateUrl: APP_CONFIG.paths.templates + '/shopping-list/list-details/list-details.template.html',
	providers: [ShoppingListService],
	pipes: [ItemStatusPipe]
})

export class ListDetailsComponent {

	@ViewChild(Content) content: Content;

	showReorder: boolean = false;	
	listRef: any;
	list: any;
	listItems: any = {
		pending: [],
		done: []
	};
	loader: any;
	status: string = 'pending';

	constructor (
		private navParams: NavParams,
		private nav: NavController,
		private alert: AlertController,
		private loading: LoadingController,
		private actionSheet: ActionSheetController,
		private popover: PopoverController,
		private service: ShoppingListService) {}

	ionViewWillEnter () {

		this.loader = this.loading.create({
			content: 'Please wait...'
		});

		//this.loader.present();
		
		this.listRef = this.service.getListById(this.navParams.get('id'));
		this.listRef.subscribe((snapshot) => {

				this.listItems = {
					pending: [],
					done: []
				};

				if (snapshot.$value === null) {
					this.nav.pop();
					return;
				}
				
				this.list = snapshot;

				if (snapshot.items) {

					this.list.items = (Object.keys(snapshot.items)).map((key) => {

						let item = snapshot.items[key];
						item.$key = key;

						this.listItems[item.checked ? 'done' : 'pending'].push(item);
						
						return item;

					});

					this.listItems = {
						pending: _.sortBy(this.listItems.pending, ['order']),
						done: _.sortBy(this.listItems.done, ['order'])
					};

				} else {

					this.status = 'pending';

				}

				this.content.resize();

				setTimeout(() => {
					//this.loader.dismiss();
				}, 1000);

			});

	}

	ionViewWillLeave () {

		this.listRef.unsubscribe();

	}

	showAddPopup () {

		let prompt = this.alert.create({
			title: 'Add New Item',
			message: "Enter a name for this item",
			inputs: [
				{
					name: 'name',
					type: 'text',
					label: 'Item Name',
					placeholder: 'Item Name',
					value: ''
				}
			],
			buttons: [
				{
					text: 'Cancel',
					handler: (data) => {
						console.log('Cancel clicked');
					}
				},
				{
					text: 'Save',
					handler: (data) => {
						if (data.name != '') {
							this.service.createItem(this.list.$key, data);
						}
					}
				}
			]
		});
	
		prompt.present();

	}

	showEditPopup (item) {

		let prompt = this.alert.create({
			title: 'Edit Item',
			message: "Enter a name for this item",
			inputs: [
				{
					name: 'name',
					type: 'text',
					label: 'Item Name',
					placeholder: 'Item Name',
					value: item.name
				}
			],
			buttons: [
				{
					text: 'Cancel',
					handler: (data) => {
						console.log('Cancel clicked');
					}
				},
				{
					text: 'Save',
					handler: (data) => {
						item.name = data.name;
						if (data.name != '') {
							this.service.updateItem(this.list.$key, item);
						}
					}
				}
			]
		});
	
		prompt.present();

	}

	showDeleteActionSheet (id: string) {
		
		let actionSheet = this.actionSheet.create({
			title: 'Confirm Delete',
			buttons: [
				{
					text: 'Delete Item',
					role: 'destructive',
					handler: () => {
						this.service.deleteItem(this.list.$key, id)
					}
				},
				{
					text: 'Cancel',
					role: 'cancel',
					handler: () => {
						console.log('Cancel clicked');
					}
				}
			]
		});
		
		actionSheet.present();

	}

	reorderItems (indexes: any) {

		this.list.items = reorderArray(this.list.items, indexes);

		this.list.items.forEach((item, i) => {
			item.order = i;
		});

		this.service.updateAllItems(this.list.$key, this.list.items);

	}

	toggleCheckItem (item: any) {

		setTimeout(() => {
			item.checked = !item.checked;
			this.service.updateItem(this.list.$key, item);
		}, 300);

	}

	showPopover (e: Event) {

		let popover = this.popover.create(ListDetailsPopover, {
			showEditListPopup: this.showEditListPopup.bind(this),
			showDeleteListActionSheet: this.showDeleteListActionSheet.bind(this)
		});

		popover.present({
			ev: e
		});

	}

	showEditListPopup () {

		let prompt = this.alert.create({
			title: 'Edit List',
			message: "Enter a name for this item",
			inputs: [
				{
					name: 'name',
					type: 'text',
					placeholder: 'List Name',
					value: this.list.name
				},
			],
			buttons: [
				{
					text: 'Cancel',
					handler: (data) => {
						console.log('Cancel clicked');
					}
				},
				{
					text: 'Save',
					handler: (data) => {

						if (data.name != '') {
							this.list.name = data.name;
							this.service.updateList(this.list);
						}
						
					}
				}
			]
		});
	
		prompt.present();

	}

	showDeleteListActionSheet () {

		let actionSheet = this.actionSheet.create({
			title: 'Confirm Delete',
			buttons: [
				{
					text: 'Delete List',
					role: 'destructive',
					handler: () => {
						this.nav.pop(); // FIX THIS
						this.service.deleteList(this.list.$key);
					}
				},
				{
					text: 'Cancel',
					role: 'cancel',
					handler: () => {
						console.log('Cancel clicked');
					}
				}
			]
		});
		
		actionSheet.present();

	}

}
