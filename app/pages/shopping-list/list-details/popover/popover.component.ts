import { Component, OnInit } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';

import { APP_CONFIG } from '../../../../config/constants.config';
import { ShoppingListService } from '../../shopping-list.service';

@Component({
	templateUrl: APP_CONFIG.paths.templates + '/shopping-list/list-details/popover/popover.template.html',
	providers: [ShoppingListService]
})

export class ListDetailsPopover {

	list: any;

	constructor (
		private navParams: NavParams,
		private viewCtrl: ViewController) {}

	ngOnInit () {

	}

	showEditPopup () {

		this.viewCtrl.dismiss();
		
		setTimeout(() => {
			this.navParams.data.showEditListPopup();
		}, 250);
		

	}

	showDeleteActionSheet () {

		this.viewCtrl.dismiss();
		
		setTimeout(() => {
			this.navParams.data.showDeleteListActionSheet();
		}, 250);

	}

}
