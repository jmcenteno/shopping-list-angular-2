import { Component, OnInit } from '@angular/core';
import { Page, NavController, ModalController } from 'ionic-angular';
import {
    AngularFire,
    firebaseAuthConfig,
    AuthProviders,
    AuthMethods,
    FirebaseAuthState
} from 'angularfire2';

import { APP_CONFIG } from '../../config/constants.config';
import { AuthService } from '../../services/auth';
import { LoginPage } from '../login';
import { ListsComponent } from '../shopping-list';

@Page({
	templateUrl: APP_CONFIG.paths.templates + '/home/home.template.html',
	providers: [
		AuthService,
        firebaseAuthConfig({
            provider: AuthProviders.Password,
            method: AuthMethods.Password,
            remember: 'default',
            scope: ['email']
        })
    ]
})

export class HomeComponent implements OnInit {
	
	constructor (
		private firebase: AngularFire,
		private nav: NavController,
		private modal: ModalController,
		private auth: AuthService) {}

	ngOnInit () {

		let loginPage = this.modal.create(LoginPage, { firebase: this.firebase });

		// subscribe to the auth object to check for the login status
        // of the user, if logged in, save some user information and
        // execute the firebase query...
        // .. otherwise
        // show the login modal page
        this.firebase.auth.subscribe((data: FirebaseAuthState) => {
            
            if (data && !data.anonymous) {

                this.firebase.auth.unsubscribe();
                this.auth.saveUserData(data);

            	if (loginPage['_loaded']) {
                	loginPage.dismiss();
                }

                setTimeout(() => {
	                this.nav.push(ListsComponent);
	            }, 1500);


            } else {

                setTimeout(() => {
                	loginPage.present();
                }, 3000);

            }

        });

	}

}
