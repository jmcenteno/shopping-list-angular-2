import { Component, OnInit } from '@angular/core';
import { 
    REACTIVE_FORM_DIRECTIVES,
    FormBuilder,
    FormControl,
    FormGroup,
    Validators
} from '@angular/forms';
import { ViewController, AlertController, LoadingController, } from 'ionic-angular';

import { APP_CONFIG } from '../../config/constants.config';
import { AuthService } from '../../services/auth';
import { EmailValidator } from '../../validators';

@Component({
	templateUrl: APP_CONFIG.paths.templates + '/register/register.template.html',
	providers: [AuthService]
})
export class RegisterComponent implements OnInit {
	
	loginForm: FormGroup;
	loader: any;
	submitted: boolean = false;

	constructor(
		private formBuilder: FormBuilder,
		private viewController: ViewController,
		private loadingController: LoadingController,
		private alertController: AlertController,
		private auth: AuthService) {}

	ngOnInit () {

		this.loginForm = this.formBuilder.group({
            email: new FormControl('', Validators.compose([
                Validators.required, 
                EmailValidator
            ])),
            password: new FormControl('', Validators.compose([
                Validators.required, 
                Validators.minLength(5)
            ])),
            confirmPassword: new FormControl('', Validators.compose([
                Validators.required, 
                Validators.minLength(5)
            ]))
        });

	}

	/**
     * this create in the user using the form credentials. 
     *
     * we are preventing the default behavor of submitting 
     * the form
     * 
     * @param _credentials {Object} the email and password from the form
     * @param _event {Object} the event information from the form submit
     */
    register (e: Event) {

    	e.preventDefault();

    	if (!this.loginForm.valid) {
    		return;
    	}

        this.loader = this.loadingController.create({
            content: 'Please wait...'
        });

        //this.loader.present();

        this.auth.createUser(this.loginForm.value)
            .then((user) => {

                //this.loader.dismiss();
           
                console.log(`Create User Success:`, user);
                this.emailLogin();

            })
            .catch((error) => {

                //this.loader.dismiss();
                console.error('Create User Failure:', error);

                let errorMessage = 'An error has occured. Please try again.';

                switch (error['code']) {
                    
                    case 'auth/email-already-in-use':
                        errorMessage = error['message'];
                        break;
                    
                }

                this.displayAlert(errorMessage);

            });

    }

    private displayAlert (error: any) {

        let alert = this.alertController.create({
            title: 'Error!',
            subTitle: error,
            buttons: ['OK']
        });
    
        alert.present();
    
    }

    /**
     * this logs in the user using the form credentials.
     * 
     * if the user is a new user, then we need to create the user AFTER
     * we have successfully logged in
     * 
     * @param _credentials {Object} the email and password from the form
     */
    private emailLogin () {

        if (!this.loginForm.valid) {
            return;
        }

        // login using the email/password auth provider
        this.auth.emailLogin(this.loginForm.value)
            .then((authData) => {
        
                this.submitted = false;
                this.viewController.dismiss();
                //this.loader.dismiss();

            })
            .catch((error) => {

                this.submitted = false;
                //this.loader.dismiss();

                let errorMessage = 'An error has occured. Please try again.';

                switch (error['code']) {

                    case 'auth/user-not-found':
                        errorMessage = 'Invalid username or password.';
                        break;

                    case 'auth/wrong-password':
                        errorMessage = 'Invalid username or password.';
                        break;

                }

                this.displayAlert(errorMessage);
                console.log(error);

            });

    }

}
