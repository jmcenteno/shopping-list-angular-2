export const APP_CONFIG: any = {
	name: 'My Shopping List',
	componentPrefix: 'sl-',
	paths: {
		templates: 'build/pages/'
	}
};

export const FIREBASE_CONFIG: any = {
	apiKey: "AIzaSyAf5o7Zd-5NYGVyXpCx5wW8ZlOZDmv__tA",
    authDomain: "shopping-list-a3e8c.firebaseapp.com",
    databaseURL: "https://shopping-list-a3e8c.firebaseio.com",
    storageBucket: "shopping-list-a3e8c.appspot.com"
};
