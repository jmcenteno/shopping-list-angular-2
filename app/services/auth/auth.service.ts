import { Injectable } from '@angular/core';
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';
import { Locker } from 'angular2-locker';
import { Facebook } from 'ionic-native';

declare var firebase: any;

@Injectable()
export class AuthService {

	constructor (
        private firebase: AngularFire,
        private localStorage: Locker) {}

    emailLogin (credentials: any) {

        return this.firebase.auth.login(credentials, {
            provider: AuthProviders.Password,
            method: AuthMethods.Password
        })
            .then((authData) => {

                return this.saveUserData(authData);

            });

    }

    facebookLogin () {

        let _authInfo

        return Facebook.login(['email'])
            .then((response) => {

                _authInfo = response;
                
                return new Promise((resolve, reject) => {
                    Facebook.api('me?fields=id,name,email,first_name,last_name,picture.width(100).height(100).as(picture_small),picture.width(720).height(720).as(picture_large)', [])
                        .then((profileData) => {
                            return resolve(profileData);
                        }, (err) => {
                            return reject(err);
                        });
                    });

            })
            .then((response) => {
        
                let credentials = (firebase.auth.FacebookAuthProvider as any).credential(_authInfo.authResponse.accessToken);

                return this.firebase.auth.login(credentials, {
                    provider: AuthProviders.Facebook,
                    method: AuthMethods.OAuthToken,
                    remember: 'default',
                    scope: ['email'],
                })
                    .then((authData) => {
                        
                        console.log('fcebook login', authData);
                        return this.saveUserData(authData);

                    })
                    .catch((error) => {
                        
                        return Promise.reject(error);

                    });

            })
            .catch((error) => { 
                
                return Promise.reject(error);

            });

    }

    googleLogin() {
        
        return new Promise((resolve, reject) => {
            
            // note for iOS the googleplus plugin requires ENABLE_BITCODE to be turned off in the Xcode
            window['plugins'].googleplus.login({
                'scopes': 'profile email', // optional, space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
                'webClientId': '1058529565889-1150s7igop9e95k67blpm4bkhmk4n89s.apps.googleusercontent.com',
                'offline': true, // optional, but requires the webClientId - if set to true the plugin will also return a serverAuthCode, which can be used to grant offline access to a non-Google server
            }, function (authData) {
                
                let provider = firebase.auth.GoogleAuthProvider.credential(authData.idToken, authData.accessToken);
                
                firebase.auth().signInWithCredential(provider).then((success) => {

                    console.log('success!', success);
                    return resolve(success);

                }, (error) => {

                    console.log('error', error);
                    return reject(error);

                });

            }, function (error) {

                return reject(error);

            });

        });

    }

    /*googleLogin () {

        return this.firebase.auth.login({
            provider: AuthProviders.Google,
            method: AuthMethods.OAuthToken
        }).then((authData) => {

            console.log('google login', authData);
            return this.saveUserData(authData);            

        }).catch((error) => {
            
            return Promise.reject(error);

        });

    }*/

    twitterLogin () {

        return this.firebase.auth.login({
            provider: AuthProviders.Twitter,
            method: AuthMethods.Redirect
        }).then((authData) => {

            console.log('twitter login', authData);
            return this.saveUserData(authData);            

        }).catch((error) => {
            
            return Promise.reject(error);

        });

    }

    createUser (credentials) {

        return this.firebase.auth.createUser(credentials);

    }

    saveUserData (authData) {

        const user = this.firebase.database.object('/users/' + authData.uid);
        
        user.set({
            'provider': authData.auth.providerData[0].providerId,
            'avatar': authData.auth.photoURL || 'MISSING',
            'displayName': authData.auth.providerData[0].displayName || authData.auth.email,
        });

        this.localStorage.set('user', authData.auth);

        return authData;

    }

    getAuthData () {

        let user = firebase.auth.currentUser;
        console.log(user)

        return this.localStorage.get('user');

    }

}
